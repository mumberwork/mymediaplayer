//
//  TracklistManager.swift
//  MyMediaPlayer
//
//  Created by Artur Mumber on 01.02.2022.
//

import UIKit

class TracklistMetaData {
    
    var songName: String?
    var artwork: UIImage?
    
    init(songName: String) {
        self.songName = songName
    }
    
}
