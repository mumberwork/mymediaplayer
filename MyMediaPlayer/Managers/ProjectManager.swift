//
//  ProjectManager.swift
//  MyMediaPlayer
//
//  Created by Artur Mumber on 01.02.2022.
//

import UIKit
import AVFoundation

class ProjectManager: NSObject {
    
    private static var uniqueInstance: ProjectManager?
    var tracklistMetaData = [TracklistMetaData]()
    weak var delegate: DataProtocol?
    private var audioPlayer = AVAudioPlayer()
    var tracklist = [URL]()
    private var counter = 0
    private var timer: Timer?
    var loopTrackEnabled = false
    

    static func shared() -> ProjectManager {
        if uniqueInstance == nil {
            uniqueInstance = ProjectManager()
        }
        return uniqueInstance!
    }
    
    override init() {
        super.init()
        audioPlayer.delegate = self
    }
    
    func initTrack () {
        guard let urlsSounds = Bundle.main.urls(forResourcesWithExtension: "mp3",
                                                subdirectory: nil) else { return }
        tracklist = urlsSounds
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: tracklist[counter])
            getMetaData()
            delegate?.callSongName(index: counter)
            
        } catch {
            print(error)
        }
    }
    
    func getMetaData() {
        for index in 0 ..< tracklist.count {
            var artistName = String()
            var titleName = String()
            var artwork = UIImage()
            let playerItem = AVPlayerItem(url: tracklist[index])
            let metaDataList = playerItem.asset.commonMetadata
            for data in metaDataList {
                if let value = data.value as? String {
                    if data.commonKey!.rawValue == "title" {
                        titleName = value
                        
                    }
                    if data.commonKey!.rawValue == "artist" {
                        artistName = value
                    }
                    if data.commonKey!.rawValue == "artwork" {
                        if let audioImage = UIImage(data: (value as! NSData) as Data) {
                            artwork = audioImage
                        }
                    }
                }
            }
            let fullSongName = "\(artistName) - \(titleName)"
            tracklistMetaData.append(TracklistMetaData(songName: fullSongName))
        }
    }
    
    
    func playPause() {
        guard !tracklist.isEmpty else {
            print("Please upload songs to the tracklist")
            return
        }
        if audioPlayer.isPlaying {
            audioPlayer.pause()
        } else {
            audioPlayer.play()
        }
    }
    
    func callTrack(number: Int) {
        guard !tracklist.isEmpty else {
            print("Please upload songs to the tracklist")
            return
        }
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: tracklist[number])
            counter = number
        } catch {
            print(error)
        }
        audioPlayer.delegate = self
        playPause()
        delegate?.callSongName(index: number)
    }
    
    func nextTrack() {
        counter += 1
        if counter > tracklist.endIndex - 1 {
            counter = tracklist.startIndex
        }
        callTrack(number: counter)
    }
    
    func previousTrack() {
        counter -= 1
        if counter < tracklist.startIndex {
            counter = tracklist.endIndex - 1
        }
        callTrack(number: counter)
    }
    
    func currentTimeReproduction() -> Float {
        return Float(audioPlayer.currentTime)
    }
    
    func durationTimeReproduction() -> Float {
        return Float(audioPlayer.duration)
    }
    
    func setTimeReproduction(_ time: Float) {
        if audioPlayer.isPlaying {
            audioPlayer.pause()
            audioPlayer.currentTime = TimeInterval(time)
            audioPlayer.play()
        } else {
            audioPlayer.currentTime = TimeInterval(time)
        }
    }
    
    func formattingTimeReproduction(value: Float) -> String {
        let durationTime = Int(value)
        let minutes = durationTime / 60
        let seconds = durationTime - minutes * 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
}

extension ProjectManager: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if flag {
            if !loopTrackEnabled {
                nextTrack()
            } else {
                callTrack(number: counter)
            }
        }
    }
}

protocol DataProtocol: AnyObject {
    func callSongName(index: Int)
    
}
