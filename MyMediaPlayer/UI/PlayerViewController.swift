//
//  ViewController.swift
//  MyMediaPlayer
//
//  Created by Artur Mumber on 01.02.2022.
//

import UIKit
import AVFoundation

class PlayerViewController: UIViewController {
    
    @IBOutlet private weak var timingView: TimingView!
    
    @IBOutlet private weak var loopTrack: UISwitch!
    
    private let projectManager = ProjectManager.shared()
    
    private let tracklistVC = TracklistViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        projectManager.delegate = self
        projectManager.initTrack()
    }

    @IBAction func playPause(_ sender: UIButton) {
        projectManager.playPause()
    }
    
    @IBAction func nextTrack(_ sender: UIButton) {
        projectManager.nextTrack()
    }
    
    @IBAction func previousTrack(_ sender: UIButton) {
        projectManager.previousTrack()
    }
    
    @IBAction func switchLoop(_ sender: Any) {
        projectManager.loopTrackEnabled = loopTrack.isOn
    }
}

extension PlayerViewController: DataProtocol {
    func callSongName(index: Int) {
        timingView.songName.text = projectManager.tracklistMetaData[index].songName
        timingView.startTimer()
    }
}


