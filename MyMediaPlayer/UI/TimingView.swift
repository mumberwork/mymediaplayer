//
//  TimingView.swift
//  MyMediaPlayer
//
//  Created by Artur Mumber on 03.02.2022.
//

import UIKit

class TimingView: UIView {
    
    @IBOutlet weak var songName: UILabel!
    
    @IBOutlet private weak var progressReproductionSlider: UISlider!
    
    @IBOutlet private weak var currentTimeTrack: UILabel!
    
    @IBOutlet private weak var durationTimeTrack: UILabel!
    
    let projectManager = ProjectManager.shared()
    var timer: Timer?
    
    func startTimer() {
        timer  = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(currentProgressMusic), userInfo: nil, repeats: true)
    }
    
    @objc func currentProgressMusic() {
        currentReproductionSlider()
        currentTimeReproductionLabel()
        durationTimeReproductionLabel()
    }
    
    func currentReproductionSlider() {
        progressReproductionSlider.value = projectManager.currentTimeReproduction()
        progressReproductionSlider.maximumValue = projectManager.durationTimeReproduction()
    }

    func currentTimeReproductionLabel() {
        currentTimeTrack.text = projectManager.formattingTimeReproduction(value: progressReproductionSlider.value)
    }
    
    func durationTimeReproductionLabel() {
        durationTimeTrack.text = projectManager.formattingTimeReproduction(value: progressReproductionSlider.maximumValue)
    }
    
    @IBAction func changeProgressMusic(_ sender: Any) {
        projectManager.setTimeReproduction(self.progressReproductionSlider.value)
    }
    
}

