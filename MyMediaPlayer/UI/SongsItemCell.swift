//
//  SongsItemCell.swift
//  MyMediaPlayer
//
//  Created by Artur Mumber on 04.02.2022.
//

import UIKit

class SongsItemCell: UITableViewCell {
    
    @IBOutlet private weak var backgrView: UIView!
    @IBOutlet weak var songName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        backgrView.backgroundColor = isSelected ? #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    

}
