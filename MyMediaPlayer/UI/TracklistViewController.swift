//
//  TracklistViewController.swift
//  MyMediaPlayer
//
//  Created by Artur Mumber on 03.02.2022.
//

import UIKit

class TracklistViewController: UIViewController {
    
    private let projectManager = ProjectManager.shared()
    private let songsName = ProjectManager.shared().tracklistMetaData
    
    @IBOutlet private weak var tracklistTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tracklistTableView.delegate = self
        tracklistTableView.dataSource = self
    }
}

extension TracklistViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projectManager.tracklist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SongsItemCell
        cell.songName.text = songsName[indexPath.row].songName
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        projectManager.callTrack(number: indexPath.row)
    }
}


